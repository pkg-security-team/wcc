#!/bin/sh -e

# called by uscan with '--upstream-version' <version

# set -x

workdir=$(mktemp -d)

test "$1" = '--upstream-version'
VERSION="$2"

PKG=wcc
UPSTREAMURL=https://github.com/endrazine/wcc.git
DL_TAR=../${PKG}_$VERSION.orig.tar.xz
ORIG_TAR=../${PKG}_$VERSION+dfsg.orig.tar.xz

git -c advice.detachedHead=false clone --quiet --depth 1 --branch v"$VERSION" ${UPSTREAMURL} "${workdir}/wcc-git"
tar -C "$workdir" -xf "$DL_TAR"

(
    cd "$workdir/$PKG-$VERSION"
    rm -rf .git
    cp -alf "$workdir/$PKG-git/.git" .git
    git submodule --quiet update --init
    rm -rf .git
)

tar -C "$workdir" -cJf "$ORIG_TAR" "$PKG-$VERSION"
echo "Successfully created $ORIG_TAR" >&2

rm -f "$DL_TAR"
rm -rf "$workdir"
