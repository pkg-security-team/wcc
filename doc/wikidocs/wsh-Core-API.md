## Core API Overview

### basic functions

	help(), man()

### memory display functions
	 hexdump(), hex_dump(), hex()

### memory maps functions
	shdrs(), phdrs(), map(), procmap(), bfmap()

### symbols functions
	symbols(), functions(), objects(), info(), search(), headers()

### memory search functions
	grep(), grepptr()

### load libaries functions
	loadbin(), libs(), entrypoints(), rescan()

### code execution functions
	libcall()

### buffer manipulation functions
	xalloc(), ralloc(), xfree(), balloc(), bset(), bget(), rdstr(), rdnum()

### control flow functions
	 breakpoint(), bp()

### system settings functions
	enableaslr(), disableaslr()

### settings functions
	 verbose(), hollywood()

### advanced functions
	ltrace()


## API speficitations


### function help()

		WSH HELP FOR FUNCTION help


	NAME

		help

	SYNOPSIS

		help([topic])

	DESCRIPTION

		Display help on [topic]. If [topic] is ommitted, display general help.

	RETURN VALUES

		None


### function man()

		WSH HELP FOR FUNCTION man


	NAME

		man

	SYNOPSIS

		man([page])

	DESCRIPTION

		Display system manual page for [page].

	RETURN VALUES

		None


### function hexdump()

		WSH HELP FOR FUNCTION hexdump


	NAME

		hexdump

	SYNOPSIS

		hexdump(<address>, <num>)

	DESCRIPTION

		Display <num> bytes from memory <address> in enhanced hexadecimal form.

	RETURN VALUES

		None


### function hex_dump()

		WSH HELP FOR FUNCTION hex_dump


	NAME

		hex

	SYNOPSIS

		hex(<object>)

	DESCRIPTION

		Display lua <object> in enhanced hexadecimal form.

	RETURN VALUES

		None


### function hex()

		WSH HELP FOR FUNCTION hex


	NAME

		hex

	SYNOPSIS

		hex(<object>)

	DESCRIPTION

		Display lua <object> in enhanced hexadecimal form.

	RETURN VALUES

		None


### function shdrs()

		WSH HELP FOR FUNCTION shdrs


	NAME

		shdrs

	SYNOPSIS

		shdrs()

	DESCRIPTION

		Display ELF section headers from all binaries loaded in address space.

	RETURN VALUES

		None


### function phdrs()

		WSH HELP FOR FUNCTION phdrs


	NAME

		phdrs

	SYNOPSIS

		phdrs()

	DESCRIPTION

		Display ELF program headers from all binaries loaded in address space.

	RETURN VALUES

		None


### function map()

		WSH HELP FOR FUNCTION map


	NAME

		map

	SYNOPSIS

		map()

	DESCRIPTION

		Display a table of all the memory ranges mapped in memory in the address space.

	RETURN VALUES

		None


### function procmap()

		WSH HELP FOR FUNCTION procmap


	NAME

		procmap

	SYNOPSIS

		procmap()

	DESCRIPTION

		Display a table of all the memory ranges mapped in memory in the address space as displayed in /proc/<pid>/maps.

	RETURN VALUES

		None


### function bfmap()

		WSH HELP FOR FUNCTION bfmap


	NAME

		bfmap

	SYNOPSIS

		bfmap()

	DESCRIPTION

		Bruteforce valid mapped memory ranges in address space.

	RETURN VALUES

		None


### function symbols()

		WSH HELP FOR FUNCTION symbols


	NAME

		symbols

	SYNOPSIS

		symbols([sympattern], [libpattern], [mode])

	DESCRIPTION

		Display all the symbols in memory matching [sympattern], from library [libpattern]. If [mode] is set to 1 or 2, do not wait user input between pagers. [mode] = 2 provides a shorter output.

	RETURN VALUES

		None


### function functions()

		WSH HELP FOR FUNCTION functions


	NAME

		functions

	SYNOPSIS

		table func = functions([sympattern], [libpattern], [mode])

	DESCRIPTION

		Display all the functions in memory matching [sympattern], from library [libpattern]. If [mode] is set to 1 or 2, do not wait user input between pagers. [mode] = 2 provides a shorter output.

	RETURN VALUES

		Return 1 lua table _func_ whose keys are valid function names in address space, and values are pointers to them in memory.


### function objects()

		WSH HELP FOR FUNCTION objects


	NAME

		objects

	SYNOPSIS

		objects([pattern])

	DESCRIPTION

		Display all the functions in memory matching [sympattern]

	RETURN VALUES

		None


### function info()

		WSH HELP FOR FUNCTION info


	NAME

		info

	SYNOPSIS

		info([address] | [name])

	DESCRIPTION

		Display various informations about the [address] or [name] provided : if it is mapped, and if so from which library and in which section if available.

	RETURN VALUES

		None


### function search()

		WSH HELP FOR FUNCTION search


	NAME

		search

	SYNOPSIS

		search(<pattern>)

	DESCRIPTION

		Search all object names matching <pattern> in address space.

	RETURN VALUES

		None


### function headers()

		WSH HELP FOR FUNCTION headers


	NAME

		headers

	SYNOPSIS

		headers()

	DESCRIPTION

		Display C headers suitable for linking against the API loaded in address space.

	RETURN VALUES

		None


### function grep()

		WSH HELP FOR FUNCTION grep


	NAME

		grep

	SYNOPSIS

		table match = grep(<pattern>, [patternlen], [dumplen], [before])

	DESCRIPTION

		Search <pattern> in all ELF sections in memory. Match [patternlen] bytes, then display [dumplen] bytes, optionally including [before] bytes before the match. Results are displayed in enhanced decimal form

	RETURN VALUES

		Returns 1 lua table containing matching memory addresses.


### function grepptr()

		WSH HELP FOR FUNCTION grepptr


	NAME

		grep

	SYNOPSIS

		table match = grep(<pattern>, [patternlen], [dumplen], [before])

	DESCRIPTION

		Search <pattern> in all ELF sections in memory. Match [patternlen] bytes, then display [dumplen] bytes, optionally including [before] bytes before the match. Results are displayed in enhanced decimal form

	RETURN VALUES

		Returns 1 lua table containing matching memory addresses.


### function loadbin()

		WSH HELP FOR FUNCTION loadbin


	NAME

		loadbin

	SYNOPSIS

		loadbin(<pathname>)

	DESCRIPTION

		Load binary to memory from <pathname>.

	RETURN VALUES

		None


### function libs()

		WSH HELP FOR FUNCTION libs


	NAME

		libs

	SYNOPSIS

		table libraries = libs()

	DESCRIPTION

		Display all libraries loaded in address space.

	RETURN VALUES

		Returns 1 value: a lua table _libraries_ whose values contain valid binary names (executable/libraries) mapped in memory.


### function entrypoints()

		WSH HELP FOR FUNCTION entrypoints


	NAME

		entrypoints

	SYNOPSIS

		entrypoints()

	DESCRIPTION

		Display entry points for each binary loaded in address space.

	RETURN VALUES

		None


### function rescan()

		WSH HELP FOR FUNCTION rescan


	NAME

		rescan

	SYNOPSIS

		rescan()

	DESCRIPTION

		Re-perform address space scan.

	RETURN VALUES

		None


### function libcall()

		WSH HELP FOR FUNCTION libcall


	NAME

		libcall

	SYNOPSIS

		void *ret, table ctx = libcall(<function>, [arg1], [arg2], ... arg[6])

	DESCRIPTION

		Call binary <function> with provided arguments.

	RETURN VALUES

		Returns 2 return values: _ret_ is the return value of the binary function (nill if none), _ctx_ a lua table representing the execution context of the library call.



### function xalloc()
	No help available for function xalloc()
### function ralloc()
	No help available for function ralloc()
### function xfree()
	No help available for function xfree()
### function balloc()
	No help available for function balloc()
### function bset()
	No help available for function bset()
### function bget()
	No help available for function bget()
### function rdstr()
	No help available for function rdstr()
### function rdnum()
	No help available for function rdnum()
### function breakpoint()

		WSH HELP FOR FUNCTION breakpoint


	NAME

		breakpoint

	SYNOPSIS

		breakpoint(<address>, [weight])

	DESCRIPTION

		Set a breakpoint at memory <address>. Optionally add a <weight> to breakpoint score if hit.

	RETURN VALUES

		None


### function bp()

		WSH HELP FOR FUNCTION bp


	NAME

		bp

	SYNOPSIS

		bp(<address>, [weight])

	DESCRIPTION

		Set a breakpoint at memory <address>. Optionally add a <weight> to breakpoint score if hit. Alias for breakpoint() function.

	RETURN VALUES

		None


### function enableaslr()

		WSH HELP FOR FUNCTION enableaslr


	NAME

		enableaslr

	SYNOPSIS

		enableaslr()

	DESCRIPTION

		Enable Address Space Layout Randomization (requires root privileges).

	RETURN VALUES

		None


### function disableaslr()

		WSH HELP FOR FUNCTION disableaslr


	NAME

		disableaslr

	SYNOPSIS

		disableaslr()

	DESCRIPTION

		Disable Address Space Layout Randomization (requires root privileges).

	RETURN VALUES

		None


### function verbose()

		WSH HELP FOR FUNCTION verbose


	NAME

		verbose

	SYNOPSIS

		verbose(<verbosity>)

	DESCRIPTION

		Change verbosity setting to <verbosity>.

	RETURN VALUES

		None


### function hollywood()

		WSH HELP FOR FUNCTION hollywood


	NAME

		hollywood

	SYNOPSIS

		hollywood(<level>)

	DESCRIPTION

		Change hollywood (fun) display setting to <level>, impacting color display (enable/disable).

	RETURN VALUES

		None


